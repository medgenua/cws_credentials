#!/bin/perl
# change this if you want to place this file somewhere else
# credpath is path of this pl script.
$credpath =~ s/(.*)PerlDatabaseCredentials\.pl/$1/;
my $file = "$credpath.credentials";

# read in config file
my %config;
open CONFIG, $file or die "can't open $file: $!";
while (<CONFIG>) {
    chomp;
    s/#.*//; # Remove comments
    s/^\s+//; # Remove opening whitespace
    s/\s+$//;  # Remove closing whitespace
    next unless length;
    my ($key, $value) = split(/\s*=\s*/, $_, 2);
    $config{$key} = $value;
}
close CONFIG;
## username and password 
$userid = $config{'DBUSER'};
$userpass = $config{'DBPASS'};
$host = $config{'DBHOST'};
$scriptuser = $config{'SCRIPTUSER'};
$scriptpass= $config{'SCRIPTPASS'};
## paths
$sitedir = $config{'SITEDIR'};
$sitedir =~ s/\/$//;
$scriptdir = $config{'SCRIPTDIR'};
$scriptdir =~ s/\/$//;
$scriptdirreg = $scriptdir;
$scriptdirreg =~ s/\//\\\//g;
$datadir = $config{'DATADIR'};
$datadir =~ s/\/$//;
$datadirreg =~ s/\//\\\//g;
$plotdir = $config{'PLOTDIR'};
$plotdir =~ s/\/$//;
$maintenancedir = $config{'MAINTENANCEDIR'};
$basepath = $config{'WEBPATH'};
# Decipher Credentials
$decuser = $config{'DECIPHERUSER'};
$decpass = $config{'DECIPHERPASS'};
$decgpgpass = $config{'DECIPHERGPGPASS'};
# admin email 
$adminemail = $config{'ADMINMAIL'};
#cluster settings
$cjo = $config{'CJO'};
$qsubscripts = $config{'SUBMITSCRIPTS'};
$queuename = $config{'QUEUENAME'};
$setwalltime = $config{'SETWALLTIME'};
$hpcaccount = $config{'HPC_ACCOUNT'};

## set path
# SET PATH
our $path = `echo \$PATH`;
chomp($path);
if ($config{'PATH'} ne '') {
        $path = $config{'PATH'}.":$path";
}

## you need to manually connect to DB, as it requires host (set here) as well as db-name (not set here) ! 

# exit with success code
1;
