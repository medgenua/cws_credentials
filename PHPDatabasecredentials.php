<?php
$sessionid = session_id();
if (empty($sessionid)) {
	session_start();
}
// change this line if you want to place this file somewhere else !
$file=realpath(dirname(__FILE__))."/.credentials";
$lines = file($file) or die("Could not read $file"); //return(0);
$config = array();
foreach ($lines as $line_num => $line) {
  # Comment?
  if ( ! preg_match("/#.*/", $line) ) {
    # Contains non-whitespace?
    if ( preg_match("/\S/", $line) ) {
      list( $key, $value ) = explode( "=", trim( $line ), 2);
      $config[$key] = $value;
    }
  }
}

## users and passwords
$credentialsfile = $file;
$user = $config['DBUSER'];
$pw = $config['DBPASS'];
$host = $config['DBHOST'];
$scriptuser = $config['SCRIPTUSER'];
$scriptpass = $config['SCRIPTPASS'];
// base path
$basepath = $config['WEBPATH'];
## admin email
$adminemail = $config['ADMINMAIL'];
## paths
$plotdir = $config['PLOTDIR'];
$plotdir = preg_replace('%(.*)/$%',"$1",$plotdir);
$scriptdir = $config['SCRIPTDIR'];
$scriptdir = preg_replace('%(.*)/$%',"$1",$scriptdir);
$scriptdirreg = preg_replace('%/%','\/',$scriptdir);
$sitedir = $config['SITEDIR'];
$sitedir = preg_replace('%(.*)/$%',"$1",$sitedir);
$maintenancedir = $config['MAINTENANCEDIR'];
// HPC settings
$usedrmaa = $config['USE_DRMAA'];
if (isset($config['QUEUENAME'])) {
	$queuename = $config['QUEUENAME'];
}
else {
	$queuename = '';
}
if (isset($config['USE_TRIPOD']) && $config['USE_TRIPOD'] == 1) {
	$usetripod = 1;
}
else {
	$usetripod = 0;
}
// connect to database
$mysql_access = mysql_connect($host,$user,$pw) or die("Could not connect to $host"); //return(0);
if (isset($_GET['page'])) {
	$page = $_GET['page'];
}
else {
	$page = 'main';
}
## webonly : if true, items are hidden from the interface.
if (isset($config['WEBONLY'])) {
	$webonly = $config['WEBONLY'];
}
else {
	$webonly = 0;
}
if ($page == '' || !isset($_SESSION['dbname']) ) {
	mysql_select_db('GenomicBuilds');
	$query = mysql_query("SELECT name, StringName, Description FROM CurrentBuild LIMIT 1");
	$row = mysql_fetch_array($query);
	$_SESSION['dbname'] = $row['name'];
	$_SESSION['dbstring'] = $row['StringName'];
	$_SESSION['dbdescription'] = stripslashes($row['Description']);
}

